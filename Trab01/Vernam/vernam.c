#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char alfabeto(int c)
{
	char value;
	if(c>=65 && c<=90) value = c-65;
	else if(c>=97 && c<=122) value = c-71;
	else value = c+4;
	return value;
}

char parse(int c)
{
	char value;
	if(c >= 0 && c < 26) value = c+65;
	else if(c >= 26 && c < 52) value = c+71;
	else value = c-4;

	return value;
}

int main(int argc, char* argv[])
{
	if(strcmp (argv[1],"-c") == 0)
	{
		FILE *texto_aberto = fopen(argv[3],"r");
		int size = 0;
		char ch;

		while( (ch=fgetc(texto_aberto))!= EOF )
		{
			if((ch>=65 && ch<=90) || (ch>=97 && ch<=122) || (ch>=48 && ch<=57)) size++;
		}
		int key[size];

		FILE *chave = fopen(argv[2], "w");
		for (int i=0; i< size; i++)
		{
			int aux = rand() % 62;
			char aux1;
			aux1 = parse(aux);
			key[i] = aux;
			putc(aux1, chave);
		}
		fclose(chave);
		fclose(texto_aberto);

		int cont=0;
		texto_aberto = fopen(argv[3],"r");
		FILE *texto_cifrado = fopen(argv[4],"w");
		while( (ch=fgetc(texto_aberto))!= EOF )
		{
			char new_char;
			int iaux = key[cont];
			if((ch>=65 && ch<=90) || (ch>=97 && ch<=122) || (ch>=48 && ch<=57))
			{	
				char alf;
				//int c = atoi(ch);
				alf = alfabeto(ch);
				int aux = (alf+iaux)%62;
				new_char = parse(aux);
				cont++;
			}
			else new_char = ch;

			putc(new_char, texto_cifrado);
		}
		fclose(texto_cifrado);
		fclose(texto_aberto);
		
		FILE *decifrar = fopen("decifrado.txt","w");
		texto_cifrado = fopen(argv[4], "r");
		cont = 0;
		while( (ch=fgetc(texto_cifrado))!= EOF )
		{
			char new_char;
			int iaux = key[cont];
			if((ch>=65 && ch<=90) || (ch>=97 && ch<=122) || (ch>=48 && ch<=57))
			{
				char alf;
				alf = alfabeto(ch);
				int aux;
				aux = alf - iaux;
				if (aux < 0) aux = aux+62;
				new_char = parse(aux);
				cont++;
			}
			else new_char = ch;
			putc(new_char, decifrar);
		}
		fclose(decifrar);
		fclose(texto_cifrado);
	}
	
}