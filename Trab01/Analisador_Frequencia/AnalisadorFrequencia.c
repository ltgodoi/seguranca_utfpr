#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char tabela [63];
int tabela_valor[63];

void preencherTabela()
{
	FILE *tab = fopen("tabela.txt","r");
	int i = 0;
	char ch;
	while( (ch=fgetc(tab))!= EOF )
    {
    	tabela[i] = ch;
    	tabela_valor[i] = 0;
    	i++;
    }
    fclose(tab);
}


int main(int argc, char* argv[])
{
	preencherTabela();
	FILE *texto_cifrado = fopen("texto-cifrado.txt","r");
	char ch;
	while( (ch=fgetc(texto_cifrado))!= EOF )
	{
		for (int i = 0; i<63; i++)
		{
			if(tabela[i] == ch)
			{
				int valor = tabela_valor[i];
				valor++;
				tabela_valor[i] = valor;
				break;
			}
		}
	}
	for (int i = 0; i<63; i++) printf("%c - %d \n", tabela[i], tabela_valor[i]);

	return 0;
}