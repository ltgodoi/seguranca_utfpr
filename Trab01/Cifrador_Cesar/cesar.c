#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[])
{
        if (argc < 6 )
        {
                printf ("ERRO!\nFalta de Parametros\n");
        }
        else
        {
                if (strcmp (argv[1],"-c") == 0)
                {       
                        int k = atoi(argv[3]);
                        FILE *texto_aberto = fopen(argv[4],"r");
                        char ch;

                        FILE *texto_cifrado;

                        texto_cifrado = fopen(argv[5], "w");

                        int aux1 = 6;
                        int aux2 = 75;
                        int aux3 = 7;
                        char new_char;
                        if(k >= 63) k = k%63;
                        
                        while( (ch=fgetc(texto_aberto))!= EOF )
                        {
                                if(ch >= 65 && ch <= 90)
                                {
                                        if ((ch+k) > 90 && (ch+k+aux1) < 123)   new_char = ch + k + aux1;
                                        else
                                        {
                                                if((ch+k+aux1)>=123)
                                                {
                                                        if(((ch+k+aux1)-aux2) >= 58)    new_char = (ch + k + aux1) - aux2 + aux3;
                                                        else    new_char = (ch + k + aux1)- aux2;
                                                        
                                                }
                                                else    new_char = ch + k;
                                        }
                                }
                                else if (ch >= 97 && ch <= 122)
                                {
                                        if((ch+k)>=123)
                                        {
                                                if(((ch+k)-aux2) >= 58) new_char = (ch + k) - aux2 + aux3;
                                                else    new_char = (ch + k) - aux2;
                                        }
                                        else    new_char = ch +k;
                                }
                                else if (ch >= 48 && ch <= 57)
                                {
                                        if((ch+k) >= 58)
                                        {
                                                if((ch+k+aux3+aux1) > 122)      new_char = (ch + k + aux3 + aux1) - aux2;
                                                else if ((ch+k+aux3) > 90 && (ch+k+aux3+aux1) < 123)    new_char = (ch + k + aux3) + aux1;
                                                else    new_char = (ch + k) + aux3;                                                
                                        }
                                        else    new_char = ch + k;
                                }
                                else    new_char = ch;

                                putc(new_char, texto_cifrado);
                        }

                        fclose(texto_cifrado);
                        fclose(texto_aberto);

                }
                else if (strcmp (argv[1],"-d") == 0)
                {
                        int k = atoi(argv[3]);
                        FILE *texto_cifrado = fopen(argv[4],"r");
                        char ch;

                        FILE *texto_aberto;

                        texto_aberto = fopen(argv[5], "w");

                        int aux1 = 6; 
                        int aux2 = 75;
                        int aux3 = 7;
                        char new_char;
                        if(k >= 63) k = k%63;

                        while( (ch=fgetc(texto_cifrado))!= EOF )
                        {
                                if(ch >= 65 && ch <= 90)
                                {
                                        if((ch-k) < 65)
                                        {
                                                if ((ch-k)-aux3 <= 57 && (ch-k)-aux3 >= 48) new_char = ch-k-aux3;
                                                else if ((ch-k-aux3) + aux2 >= 97) new_char = ch-k-aux3+aux2;
                                                else new_char = ch-k-aux3+aux2-aux1;
                                        }
                                        else new_char = ch-k;
                                }
                                else if (ch >= 97 && ch <= 122)
                                {
                                        if ((ch-k) < 97)
                                        {
                                                if ((ch-k)-aux1 >= 65 && (ch-k)-aux1 <= 90) new_char = ch-k-aux1;
                                                else if ((ch-k-aux1)-aux3 >=48) new_char = ch-k-aux1-aux3;
                                                else new_char = ch-k-aux1-aux3+aux2;
                                        }
                                        else new_char = ch-k;
                                }
                                else if (ch >= 48 && ch <= 57)
                                {
                                        if ((ch-k) < 48)
                                        {
                                                if ((ch-k)+aux2 >= 97) new_char = ch-k+aux2;
                                                else if ((ch-k+aux2)-aux1 >= 65) new_char = ch-k+aux2-aux1;
                                                else new_char = ch-k+aux2-aux1-aux3;
                                        }
                                        else new_char = ch-k;
                                }
                                else
                                {
                                        new_char = ch;
                                }
                                putc(new_char, texto_aberto);
                        }
                        fclose(texto_aberto);
                        fclose(texto_cifrado);
                }
                else
                {
                        printf("Parametro de codificação ou decodificacao incorreto");
                }
        }
        return 0;
}
