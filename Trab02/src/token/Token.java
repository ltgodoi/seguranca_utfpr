package token;

import hash.Hash;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ltgodoi
 */
public class Token 
{
    private String user, pass;
    private final String path = "/home/ltgodoi/NetBeansProjects/Trab02/arquivo";
    public Token(String user, String pass)
    {
        this.user = user;
        this.pass = pass;
    }
        
    public void gerarSemente() throws FileNotFoundException, IOException
    {
        File semente = new File(path + "/semente.txt"); 
        FileOutputStream fos1 = new FileOutputStream(semente);
        fos1.write((user + " - " + pass + "\n").getBytes());
        fos1.close();
    }
    
    public ArrayList gerarHash() throws IOException, UnsupportedEncodingException, NoSuchAlgorithmException
    {
        SimpleDateFormat h = new SimpleDateFormat("HH:mm");
        SimpleDateFormat d = new SimpleDateFormat("dd/MM/yyyy");
        Date hora = Calendar.getInstance().getTime();
        Date data = Calendar.getInstance().getTime();
        String horaFormatada = h.format(hora);
        String dataFormatada = d.format(data);
        
        ArrayList lista = new ArrayList();

        String currentHash = pass;
        for (int i=0; i<5; i++)
        {
            String text = Hash.md5(currentHash+horaFormatada+dataFormatada);
            text = text.substring(0, 4);
            lista.add(text);
            currentHash = Hash.md5(currentHash);
        }
        
        return lista;
    }
}
