package app;

import hash.Hash;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author ltgodoi
 */
public class Controle 
{
    private String arquivo = "/home/ltgodoi/NetBeansProjects/Trab02/arquivo/semente.txt";
    private ArrayList listaUtilizados = new ArrayList();
    private String min="";
    
    public Controle(){}
    public boolean getUser(String user) throws IOException
    {
        boolean bool = false;
        FileReader arq = new FileReader(arquivo);
        BufferedReader lerArq = new BufferedReader(arq);
        String linha = lerArq.readLine(); //le a primeira linha
        int pos = linha.indexOf(" ");
        String usr = linha.substring(0,pos);
        if(user.equals(usr)) bool = true;
        
        return bool;
    }
    
    public ArrayList token() throws FileNotFoundException, IOException, UnsupportedEncodingException, NoSuchAlgorithmException
    {
        String semente;
        FileReader arq = new FileReader(arquivo);
        BufferedReader lerArq = new BufferedReader(arq);
        String linha = lerArq.readLine();
        int pos = linha.indexOf("-");
        semente = linha.substring(pos+2);
        
        SimpleDateFormat h = new SimpleDateFormat("HH:mm");
        SimpleDateFormat d = new SimpleDateFormat("dd/MM/yyyy");
        Date hora = Calendar.getInstance().getTime();
        Date data = Calendar.getInstance().getTime();
        String horaFormatada = h.format(hora);
        String dataFormatada = d.format(data);
        
        ArrayList lista = new ArrayList();

        String currentHash = semente;
        for (int i=0; i<5; i++)
        {
            String text = Hash.md5(currentHash+horaFormatada+dataFormatada);
            text = text.substring(0, 4);
            lista.add(text);
            currentHash = Hash.md5(currentHash);
        }
        return lista;
    }
    
    public boolean verificar(String pass) throws IOException, FileNotFoundException, UnsupportedEncodingException, NoSuchAlgorithmException
    {
        boolean bool = false;
        int aux=0;
        ArrayList lista = new ArrayList();
        lista = token();
        String minCurrent;
        SimpleDateFormat h = new SimpleDateFormat("mm");
        Date hora = Calendar.getInstance().getTime();
        minCurrent = h.format(hora);
        
        if(!minCurrent.equals(min))
        {
            min = minCurrent;
            listaUtilizados.clear();
        }
        
        if(listaUtilizados.size() < 5)
        {
            for(int i = 0; i<lista.size(); i++)
            {
                for(int j = 0; j<listaUtilizados.size(); j++)
                {
                    if(listaUtilizados.get(j).toString().equals(lista.get(i).toString()))
                        lista.remove(i);
                }
            }
            for(int i=0; i<lista.size(); i++)
            {
                if(lista.get(i).toString().equals(pass))
                {
                    bool = true;
                    aux = i;
                    break;
                }
            }
            if(bool)
            {
                for(int i=aux; i<lista.size(); i++)
                {
                    listaUtilizados.add(lista.get(i).toString());
                }

            }
        }
        return bool;
    }
}
