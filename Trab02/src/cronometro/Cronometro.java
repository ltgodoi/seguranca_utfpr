package cronometro;

import app.Controle;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import token.Token;
import token.Token_Frame;

/**
 *
 * @author ltgodoi
 */
public class Cronometro implements Runnable
{
    public Cronometro(){}
    private final String path = "/home/ltgodoi/NetBeansProjects/Trab02/arquivo";
    
    public String getUser() throws IOException
    {
        FileReader arq = new FileReader(path + "/semente.txt");
        BufferedReader lerArq = new BufferedReader(arq);
        String linha = lerArq.readLine(); //le a primeira linha
        int pos = linha.indexOf(" ");
        String usr = linha.substring(0,pos);
        return usr;
    }
    
    public String getSemente() throws FileNotFoundException, IOException
    {
        String token;
        FileReader arq = new FileReader(path + "/semente.txt");
        BufferedReader lerArq = new BufferedReader(arq);
        String linha = lerArq.readLine();
        int pos = linha.indexOf("-");
        token = linha.substring(pos+2);
        return token;
    }
    
    public void up(String user, String pass)
    {
        ArrayList lista = new ArrayList();
        Token token = new Token(user, pass);
        try {
            lista = token.gerarHash();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Token_Frame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | NoSuchAlgorithmException ex) {
            Logger.getLogger(Token_Frame.class.getName()).log(Level.SEVERE, null, ex);
        }
        Token_Frame.labelPass1.setText(lista.get(0).toString());
        Token_Frame.labelPass2.setText(lista.get(1).toString());
        Token_Frame.labelPass3.setText(lista.get(2).toString());
        Token_Frame.labelPass4.setText(lista.get(3).toString());
        Token_Frame.labelPass5.setText(lista.get(4).toString());
        
    }
    
    @Override
    public void run() 
    {
        SimpleDateFormat h = new SimpleDateFormat("ss");
        Date min = Calendar.getInstance().getTime();
        String minFormat = h.format(min);
        int seg = Integer.parseInt(minFormat);
        int aux = 60 - seg;
        
        try 
        {
            up(getUser(), getSemente());
        } catch (IOException ex) {
            Logger.getLogger(Cronometro.class.getName()).log(Level.SEVERE, null, ex);
        }
        //marcando 1 minuto para validade de senha
        for (int i = aux; i>=0 ; i--) 
        {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Cronometro.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
     
        //chama novamente o inicio do cronometro
        run();        
    }
    
}
