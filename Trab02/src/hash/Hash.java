/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hash;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.security.provider.MD5;

/**
 *
 * @author ltgodoi
 */
public class Hash 
{
    public static String md5(String senha) throws UnsupportedEncodingException, NoSuchAlgorithmException
    {
        String sen;
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(MD5.class.getName()).log(Level.SEVERE, null, ex);
        }
        BigInteger hash = new BigInteger(1, md.digest(senha.getBytes("UTF-8")));  
        sen = hash.toString(16);
        
        return sen;
    }
    
}
