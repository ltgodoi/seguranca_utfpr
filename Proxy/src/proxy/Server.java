package proxy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ltgodoi
 */
public class Server extends Thread
{
    private Socket socket;
    private InputStream entradaServer;
    private OutputStream saidaServer;
    private BufferedReader read;
    
    public Server(Socket s)
    {
        //RECEBE O SOCKET POR PARAMETRO
        this.socket=s;
    }
    
    public void run()
    {
        try 
        {
            //INICIO DO SERVIDOR
            entradaServer = socket.getInputStream();
            //BUFFER DE LEITURA PARA DESCOBRIR O ENDEREÇO SOLICITADO
            read = new BufferedReader(new InputStreamReader(entradaServer));
            //CHAMA O METODO GET_ENDERECO PARA FAZER O SPLIT DO ENDEREÇO 
            //E PEGAR SÓ A PARTE QUE INTERESSA
            String endereco = getEndereco(read.readLine());
            
            //SERVIRÁ PARA ESCREVER NO BROWSER
            saidaServer = socket.getOutputStream();
            saidaServer.flush();
            
            //PRINTSTREAM PARA PODER SER PRINTADO NO BROWSER O VETOR DE BYTES
            PrintStream writer = new PrintStream(saidaServer);
            
            //VERIFICA SE ENCONTRA A PALAVRA "MONITORANDO" NA REQUISIÇÃO
            if(verificaMonitorando(endereco))
            {
                String negado = "Acesso não autorizado";
                writer.write(negado.getBytes());
                writer.flush();
                writer.close();
            }
            else
            {
                //CRIA UMA URL DO ENDEREÇO
                URL url = new URL(endereco);
                //ESTABELECE A CONEXÃO HTTP
                URLConnection connection = url.openConnection();
                connection.setDoOutput(true);

                
                String decodedString;
                byte[] b = new byte[1];
                int i = 0;
                //DO WHILE PARA PRIMEIRO ENTRAR E PEGAR O VALOR DE I REAL E DEPOIS COMPARAR...
                do
                {
                    //LENDO OS BYTES
                    i=connection.getInputStream().read(b,0,1);
                    //ESCREVENDO OS BYTES
                    writer.write(b);
                }while(i>0);
                //FECHA O ESCRITOR
                writer.close();
            }
            //Fecha o fluxo o socket e o serverSocket
            read.close();
            saidaServer.close();
            socket.close();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //METODO PARA FAZER O SPLIT DO ENDEREÇO E PEGAR A PARTE IMPORTANTE NESSE CASO
    public String getEndereco(String endereco)
    {
        System.out.println(endereco);
        String aux[] = new String[2];
        aux = endereco.split(" ");
        endereco = aux[1];
        endereco = endereco.trim();
        return endereco;
    }
    
    //METODO PARA BUSCAR A PALAVRA "MONITORANDO"
    public boolean verificaMonitorando(String entrada)
    {
        boolean bool = false;
        if(entrada.toLowerCase().contains("monitorando"))
            bool = true;
        return bool;
    }
}
