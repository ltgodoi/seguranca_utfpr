/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ltgodoi
 */
public class ProxyTrab04 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        try 
        {
            ServerSocket server = new ServerSocket(6789);
            while(true)
            {
                Socket socket = server.accept();
                new Server(socket).start();
            }

        } catch (IOException ex) {
            Logger.getLogger(ProxyTrab04.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
